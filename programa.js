"use strict";
const viewHeight = 500;


// CONTRUCCION DE GRAFICAS 2D EN CANVAS:
function Graph2D(canvasElement){
    // REFERENCIAS NECESARIAS
    this.context = canvasElement.getContext("2d");
    // THIS. Toma LA REFERENCIA DE UN OBJETO DE ORIGEN

    // ESTABLECER LOS VALORES PREDETERMINADOS
    this.minX = -10; // VALOR MINIMO EN X
    this.maxX = 10; // VALOR MAXIMO EN X
    this.minY = -10; // VALOR MINIMO Y
    this.maxY = 10; // VALOR MINIMO EN Y
    this.baseColor = "rgb(50, 50, 50)"; //COLOR CUADRICULA
    this.xColor = "rgb(100, 25, 25)"; // COLOR EJE X 
    this.yColor = "rgb(25, 100, 25)"; // COLOR EJE Y
    this.riemannColor1 = "rgb(0,100,255)"; //colores de las particiones color azul
    this.riemannColor2 = "rgb(0,255,100)"; //colores de las paticiones color verde

    
    
    // Dibuja la cuadrícula de la ventana gráfica y borra cualquier contenido anterior.:
    this.drawGrid = function(){
        this.context.clearRect(0, 0, viewHeight * 2, viewHeight); //ClearRect, BORRA UN RECTANGULA DE LA SECCION DADA
        //viewHeihgt, centrar verticalmente
        let ppuY = viewHeight / (this.maxY - this.minY); //DIBUJA LOS PIXELES POR CADA UNIDAD DE PANTALLA EN EL EJE Y y X 
        let ppuX = (viewHeight * 2) / (this.maxX - this.minX); //De arriba a abajo y de derecha a izquierda

        let i;
        for(i = Math.ceil(this.minX); i <= this.maxX; i++){ //DIBUJAR LINEA HORIZONTAL DE IZQUIERDA A DERECHA; MATH.CEIL REDONDEA UN NUMERO
            if (i != 0){
                this.context.beginPath();
                this.context.moveTo(-(this.minX - i) * ppuX, 0); //moveTo, mueve el objeto a un punto específico sin crear una línea
                this.context.lineTo(-(this.minX - i) * ppuX, viewHeight)//lineTo agrega un nuevo punto
                this.context.strokeStyle = this.baseColor; //strokeStyle color, degradado o patrón utilizado para trazos.
                this.context.lineWidth = "2";
                this.context.stroke();
            }
        }
        for(i = Math.floor(this.maxY); i >= this.minY; i--){ //DIBUJAR UNA LINEA VERTICAL EN EL EJE Y.math.floor redondea el decimal al entero mayor de el numero dado
            if (i != 0){
                this.context.beginPath();
                this.context.moveTo(0, (this.maxY - i) * ppuY);
                this.context.lineTo(viewHeight * 2, (this.maxY - i) * ppuY)
                this.context.strokeStyle = this.baseColor;
                this.context.lineWidth = "2";
                this.context.stroke();
            }
        }
        // GRAFICA FUNCION SUPERIOR 
        if (this.minX <= 0){ //si el eje X está dentro de la ventana gráfica, dibuje
            this.context.beginPath();
            this.context.moveTo(-(this.minX) * ppuX, 0);
            this.context.lineTo(-(this.minX) * ppuX, viewHeight);
            this.context.strokeStyle = this.yColor;
            this.context.lineWidth = "4";
            this.context.stroke();
        }
        if (this.minY <= 0){ //Si el eje y esta dentro de la ventana gráfica, dibujar
            this.context.beginPath();
            this.context.moveTo(0, this.maxY * ppuY);
            this.context.lineTo(viewHeight * 2, this.maxY * ppuY);
            this.context.strokeStyle = this.xColor;
            this.context.lineWidth = "4";
            this.context.stroke();
        }
    };

    // RELLENO DE RECTANGULOS PARA UNA REGION ESPECIFICA
    this.fillRegion = function(x1, y1, x2, y2){
        let ppuX = (viewHeight * 2) / (this.maxX - this.minX); // PIXELES POR REGION 
        let ppuY = viewHeight / (this.maxY - this.minY);

        let width = ppuX * (x2 - x1); // ANCHO 
        let height = ppuY * (y2 - y1); // ALTO 
        let left = (x1 - this.minX) * ppuX; //BASE 
        let top = -(y2 - this.maxY) * ppuY; // ARRIBA 

        this.context.lineWidth = "1"; // (GROSOR;RELLENO,FORMA)
        this.context.beginPath();
        this.context.fillRect(left, top, width, height);
        this.context.stroke();
    };

    //DIBUJA UNA FUNCIÓN, PARABOLA 
    this.drawParabola = function(translateX, translateY, scaleX, scaleY){ // CAMBIAN LAS DIMENSIONES DE LA FUNCION
        let ppuX = (viewHeight * 2) / (this.maxX - this.minX); // PIXELES POR UNIDAD EN LA PANTALLA
        let ppuY = viewHeight / (this.maxY - this.minY);
        let unitStep = (this.maxX - this.minX) / (viewHeight * 2); // PIXELES POR UNIDAD 
        
        this.context.strokeStyle = this.funcColor; //Color de la región de la función
        this.context.moveTo(0, -(Math.pow(this.minX, 2) - this.maxY) * ppuY);// MATH.POW ELEVA LA EXPRESION DE UNA BASE
        this.context.beginPath();

        let currentStep = this.minX;
        while(currentStep <= this.maxX){ //iterar a lo largo del eje x, un píxel a la vez para dibujar la función continua
            let nextX = (currentStep - this.minX) * ppuX;
            let nextY = (this.maxY - ((Math.pow((1/scaleX) * (currentStep - translateX), 2) * scaleY) + translateY)) * ppuY;

            this.context.lineTo(nextX, nextY);
            currentStep += unitStep; //INTERVALO DE PIXELES PARA LA FUNCION 
        }
        this.context.stroke();
    };
    //DIBUJO DE LA SUMATORIA DE RIEMANN PARABOLA
    this.drawRiemannSumParabola = function(translateX, translateY, scaleX, scaleY, partitions, XrangeMin, XrangeMax, entireOutput){
        let partitionStep = (XrangeMax - XrangeMin) / (partitions + 1); //unidades por región de partición

        let areaCalculation = "";
        let totalArea = 0;

        let currentStep = XrangeMin;
        let combinar = false; 
        let i;
        for(i = 0; i <= partitions; i++){ // Dinuja cada partición
            let partitionY = ((Math.pow((1/scaleX) * (currentStep - translateX), 2) * scaleY) + translateY);

            combinar = !combinar; //ALTERNAR LOS VALORES PARA COMBINAR LOS COLORES EN LAS PARTICIONES
            if (combinar){
                this.context.fillStyle = this.riemannColor1;
            }else{
                this.context.fillStyle = this.riemannColor2;
            }
            this.fillRegion(currentStep, 0, currentStep + partitionStep, partitionY);

            totalArea += partitionY; // Actualizar la informacion 
            if (entireOutput){
                areaCalculation += " + (" + partitionStep.toFixed(4) + " * " + partitionY.toFixed(4) + ")";
            }//si es la salida de entrada muestre el área calculada

            currentStep += partitionStep; //SIGUIENTE PARTICION 
        }

        totalArea = totalArea * partitionStep; //multiplica la suma final por el ancho de la partición para obtener el área total
        areaCalculation = totalArea.toFixed(8) + " = " + areaCalculation;

        if(entireOutput){ //devolver una salida basada en lo solicitado
            return areaCalculation;
        }
        else{
            return totalArea.toFixed(8);
        } //devolver el area total con redondeo
    };

    // Dibuja medio círculo en el lado positivo del eje x:, función número 2
    this.drawSemiCircle = function(radius){
        let ppuX = (viewHeight * 2) / (this.maxX - this.minX); // PIXELES POR UNIDAD EN PANTALLA
        let ppuY = viewHeight / (this.maxY - this.minY);
        let unitStep = (this.maxX - this.minX) / (viewHeight * 2); //PIXELES POR UNIDAD EN PANTALLA 

        this.context.strokeStyle = this.funcColor;
        this.context.moveTo((-this.minX - radius) * ppuX, this.maxY * ppuY); //desplazamiento de la función
        this.context.beginPath();

        let currentStep = (-radius) // comenzar en el cuadrante izquierdo del círculo
        while(currentStep <= radius){ // dibuje iterativamente la función paso a paso de un píxel a la vez
            let nextX = (currentStep - this.minX) * ppuX;
            let nextY = (this.maxY - Math.sqrt(radius * radius - currentStep * currentStep)) * ppuY; //función
            this.context.lineTo(nextX, nextY);

            currentStep += unitStep; // paso uno del pixel a lo largo del eje x
        }
        this.context.stroke();
    };
    // Dibujar la función de riemann para la función círculo
    this.drawRiemannSumSemiCircle = function(radius, partitions){
        let partitionStep = (2 * radius) / (partitions + 1); //Unidades por partición, fórmula general en Riemman

        let totalArea = 0;
        let currentStep = -radius;
        let combinar = false;
        let i;
        for(i = 0; i <= partitions; i++){ // dibuja la región de partición de izquierda a derecha
            let partitionY = Math.sqrt(radius * radius - currentStep * currentStep);

            combinar = !combinar; // alternar la región de colores
            if (combinar){
                this.context.fillStyle = this.riemannColor1;
            }else{
                this.context.fillStyle = this.riemannColor2;
            }
            this.fillRegion(currentStep, 0, currentStep + partitionStep, partitionY);

            totalArea += partitionY;
            currentStep += partitionStep;
        }

        totalArea = totalArea * partitionStep; //Calcular y retornar el área total (cadena completa)
        return totalArea.toFixed(8);
    };

    // Dibuja la onda seno:
    this.drawSinWave = function(){
        let ppuX = (viewHeight * 2) / (this.maxX - this.minX); // Pixeles por unidad en pantalla
        let ppuY = viewHeight / (this.maxY - this.minY);
        let unitStep = (this.maxX - this.minX) / (viewHeight * 2); // Unidades por Pixel
        
        this.context.strokeStyle = this.funcColor;
        this.context.moveTo(0, (this.maxY - Math.sin(this.minX)) * ppuY); // Calculando Función
        this.context.beginPath();

        let currentStep = this.minX;
        while(currentStep <= this.maxX){ // iterar a lo largo del eje x, por un pixel a la vez que dibuje la función continua
            let nextX = (currentStep - this.minX) * ppuX;
            let nextY = (this.maxY - Math.sin(currentStep)) * ppuY;

            this.context.lineTo(nextX, nextY);
            currentStep += unitStep; // pasar un pixel a lo largo del eje x
        }
        this.context.stroke();
    };
    // Dibujar la región de Riemann para la función Seno
    this.drawRiemannSumSinWave = function(partitions, XrangeMin, XrangeMax){
        let partitionStep = (XrangeMax - XrangeMin) / (partitions + 1); // Unidades por región de partición

        let totalArea = 0;
        let currentStep = XrangeMin;
        let combinar = false;
        let i;
        for(i = 0; i <= partitions; i++){ // Dibujar cada participación en un ciclo
            let partitionY = Math.sin(currentStep);

            combinar = !combinar; // alternar los colores de la partición
            if (combinar){
                this.context.fillStyle = this.riemannColor1;
            }else{
                this.context.fillStyle = this.riemannColor2;
            }
            this.fillRegion(currentStep, 0, currentStep + partitionStep, partitionY);

            totalArea += partitionY;
            currentStep += partitionStep; // paso para la siguiente partición
        }

        totalArea = totalArea * partitionStep; // multiplicar la suma final por el ancho de la partición
        return totalArea.toFixed(8);
    };
};

//
// Suma de Riemann iteración lógica
//

// Permite que el desbloqueamiento vertical se bloquee o desbloquee
var scrollFrozen = false;
var frozenPosition = 0;
// Vinculación al evento desplazamiento mostrado en pantalla
function onScroll(){
    if (scrollFrozen){
        window.scrollTo(0, frozenPosition);
    }
};
// Invocar una vez que el desplazamiento se deba congelar
function freezeScroll(){
    frozenPosition = window.scrollY;
    scrollFrozen = true;
};
// Invocar una vez que el desplazamiento se deba descongelar
function unfreezeScroll(){
    scrollFrozen = false;
};

// Vincula el primer control deslizante para actualizar el contenido del gráfico B y sus respectivos css
var sumSlider = document.querySelector("#sum-slider");
var sumOut = document.querySelector("#sum-output");
sumSlider.oninput = function(){
    graphB.drawGrid();
    sumOut.innerHTML = graphB.drawRiemannSumParabola(0,4,2,-1, Math.round(this.value), -4 ,4, true);
    graphB.drawParabola(0,4,2,-1);
};

// Vincula el botón de salida en la pantalla (exit)
document.querySelector("#anti-activity-button").addEventListener("click", () => {
    window.close();
});

// Definde la lógica para la actividad
var activityOpen = false;
var currentGraphFunction = 1;
var currentPartitions = 1;
var background = document.querySelector("#activity-bg"); // Exit "no me toques"
var activityGraph = new Graph2D(document.querySelector("#output-graph3")); //grafica 3
var activityInfo = document.querySelector("#activity-info"); //Información de área y particiones 
// Botón de enlace para abrir el ejercicio
document.querySelector("#activity-button").addEventListener("click", () => {
    if (!activityOpen){
        background.classList.remove("activity-hidden");
        activityOpen = true;
        freezeScroll();
    }
});
//GRAFICO DE LA SUMATORIA actividad recuadro que se agrega al presionar el botón de las 3 funciones
function redrawActivity(){
    switch(currentGraphFunction){ // BIDUJO BASADO EN CURRENT.GRAPH
        case 1:
            activityGraph.minX = -6;
            activityGraph.maxX = 6;
            activityGraph.minY = -1;
            activityGraph.maxY = 5;
            activityGraph.drawGrid();
            let areaA = activityGraph.drawRiemannSumParabola(0,4,2,-1, currentPartitions, -4 ,4, false);
            activityInfo.innerHTML = "Function: Parabola | Partitions: " + currentPartitions + " | Riemann Area: " + areaA + " | True Area: (64/3) = 21.33333333";
            activityGraph.drawParabola(0,4,2,-1);
            break;
        case 2:
            activityGraph.minX = -3;
            activityGraph.maxX = 3;
            activityGraph.minY = -1;
            activityGraph.maxY = 3;
            activityGraph.drawGrid();
            let areaB = activityGraph.drawRiemannSumSemiCircle(2, currentPartitions);
            activityInfo.innerHTML = "Function: Circle | Partitions: " + currentPartitions + " | Riemann Area: " + areaB + " | True Area: (2pi) = 6.28318531";
            activityGraph.drawSemiCircle(2);
            break;
        case 3:
            activityGraph.minX = -1;
            activityGraph.maxX = 6;
            activityGraph.minY = -2;
            activityGraph.maxY = 2;
            activityGraph.drawGrid();
            let areaC = activityGraph.drawRiemannSumSinWave(currentPartitions, 0, Math.PI);
            activityInfo.innerHTML = "Function: Sin | Partitions: " + currentPartitions + " | Riemann Area: " + areaC + " | True Area: 2";
            activityGraph.drawSinWave();
            break;
    }
};
// entrada de las techas, arriba, abajo, derecha e isquierda para la actividad
document.body.addEventListener("keydown", (e) => {
    if (activityOpen){ // cuando no se haya presionado el botón las teclas no son permitidas 
        switch(e.keyCode){
            case 27: // esc (salida)
                background.classList.add("activity-hidden");
                activityOpen = false;
                unfreezeScroll();
                break;
            case 37: // izquierda
                if (currentGraphFunction > 1){
                    currentGraphFunction--;
                    redrawActivity();
                }
                break;
            case 39: // derecha
                if (currentGraphFunction < 3){
                    currentGraphFunction++;
                    redrawActivity();
                }
                break;
            case 38: // arriba
                if (currentPartitions < 1000){
                    currentPartitions++;
                    redrawActivity();
                }
                break;
            case 40: // abajo
                if (currentPartitions > 1){
                    currentPartitions--;
                    redrawActivity();
                }
                break;
        }
    }
});

//
// Inicializar objetos de página, gráficas se reinicializan cada vez que cambiamos de función
//

var graphA = new Graph2D(document.querySelector("#output-graph"));
graphA.minX = -6;
graphA.maxX = 6;
graphA.minY = -1;
graphA.maxY = 5;
graphA.drawGrid();
graphA.drawParabola(0,4,2,-1);

var graphB = new Graph2D(document.querySelector("#output-graph2"));
graphB.minX = -6;
graphB.maxX = 6;
graphB.minY = -1;
graphB.maxY = 5;
graphB.drawGrid();
graphB.drawRiemannSumParabola(0,4,2,-1, 9, -4 ,4, false);
graphB.drawParabola(0,4,2,-1);

window.addEventListener("scroll", onScroll);

redrawActivity();
